package com.hw.db.DAO;

import com.hw.db.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Optional;
import java.util.stream.Stream;

public class UserDAOTests {
    private JdbcTemplate mockJdbc;

    @BeforeEach
    void setUp() {
        mockJdbc = Mockito.mock(JdbcTemplate.class);
        new UserDAO(this.mockJdbc);
    }

    private static Stream<Arguments> params() {
        return Stream.of(
            Arguments.of(null, null, "about", "UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"), 
            Arguments.of(null, "fullName", null, "UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"), 
            Arguments.of(null, "fullName", "about", "UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"), 
            Arguments.of("email", null, null, "UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"), 
            Arguments.of("email", null, "about", "UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"), 
            Arguments.of("email", "fullName", null, "UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"), 
            Arguments.of("email", "fullName", "about", "UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;")
            );
    }

    @ParameterizedTest
    @MethodSource("params")
    void TestChange(String email, String fullname, String about, String q) {
        UserDAO.Change(new User("Noelliya", email, fullname, about));
        Mockito.verify(mockJdbc).update(Mockito.eq(q), Optional.ofNullable(Mockito.any()));
    }

    @Test
    void TestNoChange() {
        UserDAO.Change(new User("Kakarina", null, null, null));
        Mockito.verify(mockJdbc, Mockito.never()).update(Mockito.anyString(), Optional.ofNullable(Mockito.any()));
    }
}